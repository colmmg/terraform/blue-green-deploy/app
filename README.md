# blue-green-deploy/app
The blue-green-deploy application Terraform code.

# Usage
```
terraform init -backend-config="region=$AWS_REGION" \
               -backend-config="key=blue-green-deploy/app/0.1.0/terraform.tfstate"
terraform plan -var-file=tfvars/$AWS_ENVIRONMENT/$AWS_REGION/terraform.tfvars
terraform apply -var-file=tfvars/$AWS_ENVIRONMENT/$AWS_REGION/terraform.tfvars
```

# License and Authors
Authors: [Colm McGuigan](https://gitlab.com/colmmg)
