variable "app_version" {
  description = "The version of this app."
}

variable "private_subnets" {
  description = "A list of subnet IDs to launch resources in. ."
  type        = "list"
}

variable "region" {
  default     = "us-east-1"
  description = "The AWS region."
}

variable "security_groups" {
  description = "A list of security group IDs to assign to the autoscaling group."
  type        = "list"
}
