provider "aws" {
  region = "${var.region}"
}

data "aws_caller_identity" "current" {}

data "aws_ami" "amzn-linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["137112412989"]
}

data "template_file" "blue-green-deploy" {
  template = "${file("${path.module}/templates/user_data.tpl")}"
  vars {
    region = "${var.region}"
  }
}

resource "aws_launch_configuration" "blue-green-deploy" {
  image_id        = "${data.aws_ami.amzn-linux.id}"
  instance_type   = "t2.micro"
  lifecycle {
    create_before_destroy = true
  }
  name_prefix     = "blue-green-deploy-"
  security_groups = [ "${var.security_groups}" ]
  user_data       = "${data.template_file.blue-green-deploy.rendered}"
}

resource "aws_autoscaling_group" "blue-green-deploy" {
  force_delete              = false
  health_check_grace_period = 300
  health_check_type         = "EC2"
  launch_configuration      = "${aws_launch_configuration.blue-green-deploy.name}"
  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      "tag",
      "suspended_processes"
    ]
  }
  min_size                  = "1"
  min_elb_capacity          = "1"
  max_size                  = "1"
  name                      = "blue-green-deploy-${var.app_version}"
  suspended_processes       = [ "AlarmNotification", "HealthCheck" ]
  tag {
    key                 = "Name"
    value               = "blue-green-deploy"
    propagate_at_launch = true
  }
  tag {
    key                 = "Version"
    value               = "${var.app_version}"
    propagate_at_launch = true
  }
  vpc_zone_identifier       = [ "${var.private_subnets}" ]
}
